# Liste du matériel à dispositon a l'ADRI38

## Mesure

### VNA / Scalaire 
- MiniVNA : 0 - 180 MHz
- DG8SAQ : 1kHz - 1.3 GHz 
- HP 8756 + 8350B  : 10 MHz <-> 2.4 GHz

### Fréquencemetre
- EIP : 20 GHz

### Générateur 
- Giga instruments GP2028 : 2 GHz - 8 GHz
- Divers générateur basse frequence : < 2 MHz

### Analyser de spéctre
- R&S : FSEK : 20 Hz - 40 GHz
- HP :
- Hameg : 500 Mhz

### Oscilloscpe 
- Plusieurs appareils sont disponible
- Sonde passive

### Analyser de composant
- Transformer

### Adaptateur
plusieurs types sont disponibles

- SMA 
- BNC
- PL
- SO
- ...


## Prototypage
- Alimentation divers
- fer a souder classique
- outil a main

## Mécanique
- outillage à main classique
- Perceuse sur colonne
